const pollBtn = document.querySelector(".poll-btn");
const overlay = document.querySelector(".overlay");
const hidden = document.querySelector("hidden");
const popup = document.querySelector(".popup");
const btnClose = document.querySelector(".btnclose");

pollBtn.addEventListener("click", function () {
  popup.classList.remove("hidden");
  popup.classList.add("overlay");
});
btnClose.addEventListener("click", function () {
  popup.classList.add("hidden");
});
